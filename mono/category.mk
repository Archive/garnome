# For mono, attempt to use the latest devel revision first (libraries, runtimes)
# then stable, then 1.0 as a sensible fallback solution for everything else 
# -- pd.
MASTER_SITES += http://www.go-mono.com/sources/$(GARNAME)/
MASTER_SITES += http://www.go-mono.com/sources/mono-1.1/
MASTER_SITES += http://www.go-mono.com/archive/1.0.6/
MASTER_SITES += http://www.go-mono.com/archive/1.0/

include ../../gar.gnome.mk
include ../../gar.mk
