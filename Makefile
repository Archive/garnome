# top-level Makefile for the entire tree.
%:
	@for i in $(filter-out CVS/ bootstrap/ broken/ evolution/ pwlib/,$(wildcard */)) ; do \
		$(MAKE) -C $$i $* ; \
	done

paranoid-%:
	@for i in $(filter-out CVS/ broken/ bootstrap/ evolution/ pwlib/,$(wildcard */)) ; do \
		$(MAKE) -C $$i $@ || exit 2; \
	done

export BUILDLOG ?= $(shell pwd)/buildlog.txt

unbuilt:
	@(find . -mindepth 3 -maxdepth 3 -name work | cut -d/ -f 3; find . -mindepth 2 -maxdepth 2 | cut -d/ -f3) | sort | uniq -u

report-%:
	@echo "$@ started at $$(date)" >> $(BUILDLOG)
	@for i in $(filter-out CVS/ broken/ bootstrap/ evolution/ pwlib/,$(wildcard */)) ; do \
		$(MAKE) -C $$i $@ || echo "	make $@ in category $$i failed" >> $(BUILDLOG); \
	done
	@echo "$@ completed at $$(date)" >> $(BUILDLOG)

STYLESHEET = http://www.gnome.org/~jdub/garnome/packages.css
HTMLDIR = ../website/root/packages
PACKAGEURL = http://www.gnome.org/~jdub/garnome/packages
HTMLINDEX = $(HTMLDIR)/index.html

export HTMLINDEX STYLESHEET 

define HTMLINDEXSTART
<html>\n
<head>\n
\t<title>Package list</title>\n
\t<link rel="stylesheet" type="text/css" href="$(STYLESHEET)">\n
</head>\n
<body>\n
<h1>List of Packages Available</h1>\n
endef

define HTMLINDEXSTOP
</body>\n
</html>
endef

export HTMLINDEXSTART HTMLINDEXSTOP PACKAGEURL

webpages:
	@mkdir -p $(HTMLDIR)
	@echo -e $${HTMLINDEXSTART} > $(HTMLINDEX).tmp
	@for i in $(filter-out CVS/ evolution/ pwlib/,$(wildcard */)) ; do \
		mkdir -p $(HTMLDIR)/$$i;\
		echo "<h2>$$i</h2>" >> $(HTMLINDEX).tmp;\
		echo "<ol>" >> $(HTMLINDEX).tmp;\
		$(MAKE) GARCATEGORY=$$i HTMLINDEX=$(HTMLINDEX).tmp HTMLDIR=$(HTMLDIR)/$$i CATEGORYURL=$(PACKAGEURL)/$$i -C $$i webpages || echo "$$i on fire."; \
		echo "</ol>" >> $(HTMLINDEX).tmp;\
	done
	@echo -e $${HTMLINDEXSTOP} >> $(HTMLINDEX).tmp
	@mv $(HTMLINDEX).tmp $(HTMLINDEX)

.PHONY: unbuilt
