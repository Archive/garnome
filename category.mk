# This makefile is to be included from Makefiles in each category
# directory.
%:
	@for i in $(filter-out CVS/,$(wildcard */)) ; do \
		$(MAKE) -C $$i $* ; \
	done

paranoid-%:
	@for i in $(filter-out CVS/,$(wildcard */)) ; do \
		$(MAKE) -C $$i $* || exit 2; \
	done

export BUILDLOG ?= $(shell pwd)/buildlog.txt

report-%:
	@for i in $(filter-out CVS/,$(wildcard */)) ; do \
		$(MAKE) -C $$i $* || echo "	*** make $* in $$i failed ***" >> $(BUILDLOG); \
	done


# Superuser sanity check.  Prevent users from accidentally building as root.
UID ?= $(shell id -u )
export UID

ifeq (${UID}, 0)
  # This file isn't supposed to be included directly from categories. However,
  # since we know we will do an emergency stop anyway, we can just a well keep
  # the error message in a single place.
  include ../gar.gnome.mk
endif

